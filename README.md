This repository is based on the https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html

As dataset we used the combined images from [FAB-M3](https://cloudstore.zih.tu-dresden.de/index.php/s/mgEPZrpkkScoAfG) and [FAB-Non-M3](https://cloudstore.zih.tu-dresden.de/index.php/s/TdacMmtexA6rTQp). We used only the images of 50x magnification and split them into 89 train and 20 test images. Because this dataset is very small, we tried to increase the sample size via image slicing and different augmentation techniques, but this did not improve the results. 

The [Notebook](https://gitlab.com/TimSchmittmann/whole-slide-aml-classification/-/blob/master/m3_other_wsi_classification.ipynb) should be able to run inside colab without any further actions. 

If you want to recreate the environment and run the notebook locally try to use 

    pip install -r requirements.txt
    
which was created from [pipreqs](https://pypi.org/project/pipreqs/). Otherwise [pipfreeze.requirements.txt](https://gitlab.com/TimSchmittmann/whole-slide-aml-classification/-/blob/master/pipfreeze.requirements.txt) contains all the installed packages from the colab environment. 